import 'package:fitbitter/src/data/fitbitData.dart';
import 'package:fitbitter/src/utils/formats.dart';

/// [FitbitWeightData] is a class implementing the data model of the
/// user heart activity data.
class FitbitWeightData implements FitbitData {
  /// The user encoded id.
  String? userID;

  /// The date of monitoring of the data.
  DateTime? dateOfMonitoring;

  /// The bmi of the data.
  double? bmi;

  /// The weight of the data.
  double? weight;

  /// Default [FitbitWeightData] constructor.
  FitbitWeightData({
    this.userID,
    this.dateOfMonitoring,
    this.bmi,
    this.weight,
  });

  /// Generates a [FitbitWeightData] obtained from a json.
  factory FitbitWeightData.fromJson({required Map<String, dynamic> json}) {
    return FitbitWeightData(
      userID: json['userID'],
      dateOfMonitoring:
          Formats.onlyDayDateFormatTicks.parse(json['dateOfMonitoring']),
      bmi: json['bmi'],
      weight: json['weight'],
    );
  } // fromJson

  @override
  String toString() {
    return (StringBuffer('FitbitHeartRateData(')
          ..write('userID: $userID, ')
          ..write('dateOfMonitoring: $dateOfMonitoring, ')
          ..write('bmi: $bmi, ')
          ..write('weight: $weight, ')
          ..write(')'))
        .toString();
  } // toString

  @override
  Map<String, dynamic> toJson<T extends FitbitData>() {
    return <String, dynamic>{
      'userID': userID,
      'dateOfMonitoring': dateOfMonitoring,
      'bmi': bmi,
      'weight': weight,
    };
  } // toJson

} // FitbitHeartRateData
