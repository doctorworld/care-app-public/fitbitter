import 'package:fitbitter/src/data/fitbitData.dart';
import 'package:fitbitter/src/utils/formats.dart';

/// [FitbitSleepLogData] is a class implementing the data model of the
/// user sleep data.
class FitbitSleepLogData implements FitbitData {
  /// The user encoded id.
  String? userID;

  /// The date of when the sleep session begun.
  DateTime? dateOfSleep;

  int? minutesDeep;

  int? minutesLight;

  int? minutesRem;

  int? minutesWake;

  List<FitbitSleepLogDetailData> details;

  int? get minutesAsleep =>
      details.map<int>((e) => e.minutesAsleep ?? 0).reduce((a, b) => a + b);

  /// Default [FitbitSleepLogData] constructor.
  FitbitSleepLogData({
    this.userID,
    this.dateOfSleep,
    this.minutesDeep,
    this.minutesLight,
    this.minutesRem,
    this.minutesWake,
    required this.details,
  });

  /// Generates a [FitbitSleepLogData] obtained from a json.
  factory FitbitSleepLogData.fromJson({required Map<String, dynamic> json}) {
    return FitbitSleepLogData(
      userID: json['userID'],
      dateOfSleep: Formats.onlyDayDateFormatTicks.parse(json['dateOfSleep']),
      minutesDeep: json['minutesDeep'],
      minutesLight: json['minutesLight'],
      minutesRem: json['minutesRem'],
      minutesWake: json['minutesWake'],
      details: (json['details'] as List<dynamic>)
          .map((e) =>
              FitbitSleepLogDetailData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  } // fromJson

  @override
  String toString() {
    return (StringBuffer('FitbitSleepLogData(')
          ..write('userID: $userID, ')
          ..write('dateOfSleep: $dateOfSleep, ')
          ..write('minutesDeep: $minutesDeep, ')
          ..write('minutesLight: $minutesLight, ')
          ..write('minutesRem: $minutesRem, ')
          ..write('minutesWake: $minutesWake, ')
          ..write('details: ${details.toString()}, ')
          ..write(')'))
        .toString();
  } // toString

  @override
  Map<String, dynamic> toJson<T extends FitbitData>() {
    return <String, dynamic>{
      'userID': userID,
      'dateOfSleep': dateOfSleep,
      'minutesDeep': minutesDeep,
      'minutesLight': minutesLight,
      'minutesRem': minutesRem,
      'minutesWake': minutesWake,
      'details': details,
    };
  } // toJson

} // FitbitSleepLogData

class FitbitSleepLogDetailData {
  String? startTime;

  String? endTime;

  String? logType;

  int? minutesAsleep;

  FitbitSleepLogDetailData({
    this.startTime,
    this.endTime,
    this.logType,
    this.minutesAsleep,
  });

  factory FitbitSleepLogDetailData.fromJson(Map<String, dynamic> json) {
    return FitbitSleepLogDetailData(
      startTime: json['startTime'],
      endTime: json['endTime'],
      logType: json['logType'],
      minutesAsleep: json['minutesAsleep'],
    );
  } // fromJson

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'startTime': startTime,
      'endTime': endTime,
      'logType': logType,
      'minutesAsleep': minutesAsleep,
    };
  } // toJson

} // Fitbit
