import 'package:fitbitter/src/data/fitbitData.dart';
import 'package:fitbitter/src/data/fitbitSleepLogData.dart';
import 'package:fitbitter/src/managers/fitbitDataManager.dart';
import 'package:fitbitter/src/urls/fitbitAPIURL.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

/// [FitbitSleepLogDataManager] is a class the manages the requests related to
/// [FitbitSleepLogData].
class FitbitSleepLogDataManager extends FitbitDataManager {
  FitbitSleepLogDataManager(
      {required String clientID, required String clientSecret})
      : super(
          clientID: clientID,
          clientSecret: clientSecret,
        );

  @override
  Future<List<FitbitData>> fetch(FitbitAPIURL fitbitUrl) async {
    // Get the response
    final response = await getResponse(fitbitUrl);

    // Debugging
    final logger = Logger();
    logger.i('$response');

    //Extract data and return them
    List<FitbitSleepLogData> sleepDataPoints = _extractFitbitSleepLogData(
        response, fitbitUrl.fitbitCredentials!.userID);
    return sleepDataPoints;
  } // fetch

  /// A private method that extracts [FitbitSleepLogData] from the given response.
  List<FitbitSleepLogData> _extractFitbitSleepLogData(
      dynamic response, String? userId) {
    final nRecords = response["sleep"].length;
    List<FitbitSleepLogData> sleepDataPoints =
        List<FitbitSleepLogData>.empty(growable: true);
    if (nRecords <= 0) return [];

    /// recordType
    /// classic: timeInBed = minutesAsleep + minutesToFallAsleep + minutesAwake
    /// stages: timeInBed = minutesAsleep + minutesAwake
    /// stages: minutesAsleep = deep + light + rem
    ///         minutesAwake = wake

    DateTime date = DateTime.parse(response["sleep"][0]["dateOfSleep"]);
    FitbitSleepLogData log =
        FitbitSleepLogData(userID: userId, dateOfSleep: date, details: []);

    for (var record = 0; record < nRecords; record++) {
      final DateTime dateOfSleep =
          DateTime.parse(response["sleep"][record]["dateOfSleep"]);
      if (!DateUtils.isSameDay(date, dateOfSleep)) {
        sleepDataPoints.add(log);
        log = FitbitSleepLogData(
            userID: userId, dateOfSleep: dateOfSleep, details: []);
      }

      final recordType = response["sleep"][record]["type"];
      if (recordType == "stages") {
        log.minutesDeep = response["sleep"][record]["levels"]?["summary"]
                ?["deep"]?["minutes"] +
            (log.minutesDeep ?? 0);
        log.minutesLight = response["sleep"][record]["levels"]?["summary"]
                ?["light"]?["minutes"] +
            (log.minutesLight ?? 0);
        log.minutesRem = response["sleep"][record]["levels"]?["summary"]?["rem"]
                ?["minutes"] +
            (log.minutesRem ?? 0);
        log.minutesWake = response["sleep"][record]["levels"]?["summary"]
                ?["wake"]?["minutes"] +
            (log.minutesWake ?? 0);

        log.details.add(FitbitSleepLogDetailData(
          startTime: response["sleep"][record]["startTime"],
          endTime: response["sleep"][record]["endTime"],
          logType: response["sleep"][record]["logType"],
          minutesAsleep: response["sleep"][record]["minutesAsleep"],
        ));
      } else {
        log.details.add(FitbitSleepLogDetailData(
          startTime: response["sleep"][record]["startTime"],
          endTime: response["sleep"][record]["endTime"],
          logType: response["sleep"][record]["logType"],
          minutesAsleep: response["sleep"][record]["minutesAsleep"],
        ));
      }
      date = dateOfSleep;
    }
    sleepDataPoints.add(log); // for nRecords
    return sleepDataPoints;
  } // _extractFitbitSleepLogData

} // FitbitSleepLogDataManager
