import 'package:fitbitter/src/data/fitbitData.dart';
import 'package:fitbitter/src/data/fitbitWeightData.dart';
import 'package:fitbitter/src/managers/fitbitDataManager.dart';
import 'package:fitbitter/src/urls/fitbitAPIURL.dart';
import 'package:fitbitter/src/utils/formats.dart';
import 'package:logger/logger.dart';

/// [FitbitWeightDataManager] is a class the manages the requests related to
/// [FitbitWeightData].
class FitbitWeightDataManager extends FitbitDataManager {
  FitbitWeightDataManager(
      {required String clientID, required String clientSecret})
      : super(
          clientID: clientID,
          clientSecret: clientSecret,
        );

  @override
  Future<List<FitbitData>> fetch(FitbitAPIURL fitbitUrl) async {
    // Get the response
    final response = await getResponse(fitbitUrl);

    // Debugging
    final logger = Logger();
    logger.i('$response');

    //Extract data and return them
    List<FitbitData> ret =
        _extractFitbitHeartData(response, fitbitUrl.fitbitCredentials!.userID);
    return ret;
  } // fetch

  /// A private method that extracts [FitbitWeightData] from the given response.
  List<FitbitWeightData> _extractFitbitHeartData(
      dynamic response, String? userId) {
    final data = response['weight'];
    List<FitbitWeightData> weightLog =
        List<FitbitWeightData>.empty(growable: true);

    for (var record = 0; record < data.length; record++) {
      weightLog.add(FitbitWeightData(
        userID: userId,
        dateOfMonitoring:
            Formats.onlyDayDateFormatTicks.parse(data[record]['date']),
        bmi: data[record]['bmi'],
        weight: data[record]['weight'] != null
            ? data[record]['weight'].toDouble()
            : null,
      ));
    } // for entry
    return weightLog;
  } // _extractFitbitHeartData

} // FitbitWeightDataManager
