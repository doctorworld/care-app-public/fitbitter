import 'package:fitbitter/src/data/fitbitHeartRateData.dart';
import 'package:fitbitter/src/fitbitConnector.dart';
import 'package:fitbitter/src/urls/fitbitAPIURL.dart';
import 'package:fitbitter/src/utils/formats.dart';

/// [FitbitWeightData] is a class that expresses multiple factory
/// constructors to be used to generate Fitbit Web APIs urls to fetch
/// [FitbitWeightData].
class FitbitWeightAPIURL extends FitbitAPIURL {
  /// Default [FitbitWeightData] constructor.
  FitbitWeightAPIURL(
      {required FitbitCredentials? fitbitCredentials, required String url})
      : super(
          url: url,
          fitbitCredentials: fitbitCredentials,
        );

  /// Generates a [FitbitWeightAPIURL] to get [FitbitWeightData] of a specific day [date].
  factory FitbitWeightAPIURL.day(
      {required FitbitCredentials fitbitCredentials, required DateTime date}) {
    String dateStr = Formats.onlyDayDateFormatTicks.format(date);
    return FitbitWeightAPIURL(
      url: '${_getBaseURL(fitbitCredentials.userID)}/date/$dateStr/1d.json',
      fitbitCredentials: fitbitCredentials,
    );
  } // FitbitHeartRateAPIURL.day

  /// Generates a [FitbitWeightAPIURL] to get [FitbitWeightData] of a specific date range
  /// between [startDate] and [endDate].
  factory FitbitWeightAPIURL.dateRange(
      {required FitbitCredentials fitbitCredentials,
      required DateTime startDate,
      required DateTime endDate}) {
    String startDateStr = Formats.onlyDayDateFormatTicks.format(startDate);
    String endDateStr = Formats.onlyDayDateFormatTicks.format(endDate);
    return FitbitWeightAPIURL(
      url:
          '${_getBaseURL(fitbitCredentials.userID)}/date/$startDateStr/$endDateStr.json',
      fitbitCredentials: fitbitCredentials,
    );
  } // FitbitHeartRateAPIURL.dateRange

  /// Generates a [FitbitWeightAPIURL] to get [FitbitHeartRateData] of a specific week
  /// ending in [baseDate].
  factory FitbitWeightAPIURL.week(
      {required FitbitCredentials fitbitCredentials,
      required DateTime baseDate}) {
    String dateStr = Formats.onlyDayDateFormatTicks.format(baseDate);
    return FitbitWeightAPIURL(
      url: '${_getBaseURL(fitbitCredentials.userID)}/date/$dateStr/1w.json',
      fitbitCredentials: fitbitCredentials,
    );
  } // FitbitWeightAPIURL.week

  /// Generates a [FitbitWeightAPIURL] to get [FitbitHeartRateData] of a specific month
  /// ending in [baseDate].
  factory FitbitWeightAPIURL.month(
      {required FitbitCredentials fitbitCredentials,
      required DateTime baseDate}) {
    String dateStr = Formats.onlyDayDateFormatTicks.format(baseDate);
    return FitbitWeightAPIURL(
      url: '${_getBaseURL(fitbitCredentials.userID)}/date/$dateStr/1m.json',
      fitbitCredentials: fitbitCredentials,
    );
  } // FitbitWeightAPIURL.month

  /// A private method that generates the base url of a [FitbitWeightAPIURL].
  static String _getBaseURL(String? userID) {
    return 'https://api.fitbit.com/1/user/$userID/body/log/weight';
  } // _getBaseURL

} // FitbitWeightAPIURL
